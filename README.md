Tap And Go
=====

Bike reservation : Symfony 3 Project

Version
----
Current Version 0.1

Server requirements
----
* Docker
* NodeJS - Yarn (or npm)

*Note : You can still deploy this app with a classic PHP5+ / Nginx / MySQL stack. If so, you'll need to set your parameters in app/config/parameters.yml, be sure to change 'sf3' alias in command by 'php bin/console' and don't mind the 'Enter Docker bash commands' steps.*

Installation
----

#### Clone BitBucket repository

```sh
git clone https://Dim4k@bitbucket.org/Dim4k/tapandgo.git
```

#### Run the server

*Build/run Docker containers*
```sh
# At the project root
cd docker-symfony
docker-compose build
docker-compose up -d
```

*Composer install and create database*
```sh
# Enter Docker bash commands
docker-compose exec php bash

# Create Symfony default parameters
cd app/config
cp parameters.yml.dist parameters.yml
cd ../../

# Install Composer dependencies
composer install --ignore-platform-reqs

# Create database
sf3 doctrine:database:create
sf3 doctrine:schema:update --force

# Exit Docker bash commands
exit
```
*Generate the assets*
```sh
# On Windows at the project root :
yarn install
node_modules\.bin\encore dev
 
# Or on Linux at the project root :
yarn install
./node_modules/.bin/encore dev
```

*Get containers IP address*
```sh
docker network inspect bridge | grep Gateway
```

Test the app
----

```sh
# Go to Docker folder
cd docker-symfony

# Enter Docker bash commands
docker-compose exec php bash

# Launch tests
php vendor/phpunit/phpunit/phpunit
```

TODO
----

* Add NodeJS to Docker
* Add DoctrineFixturesBundle to have some test data
* Rework form template
* Add user authentication