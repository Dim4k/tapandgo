<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * City
 *
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CityRepository")
 */
class City
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float
	 *
     * @ORM\Column(name="latitude", type="float")
	 *
	 * @Assert\Range(
	 *      min = -90,
	 *      max = 90,
	 *      minMessage = "Latitude must be at least {{ limit }}",
	 *      maxMessage = "Latitude must be less than {{ limit }}"
	 * )
     */
    private $latitude;

    /**
     * @var float
	 *
     * @ORM\Column(name="longitude", type="float")
	 *
	 * @Assert\Range(
	 *      min = -180,
	 *      max = 180,
	 *      minMessage = "Longitude must be at least {{ limit }}",
	 *      maxMessage = "Longitude must be less than {{ limit }}"
	 * )
     */
    private $longitude;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

	/**
	 * @ORM\OneToMany(targetEntity="Station", mappedBy="city")
	 */
	private $stations;

	public function __construct()
	{
		$this->stations = new ArrayCollection();
	}

	public function __toString() {
		return $this->name;
	}

	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return City
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return City
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return City
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add station
     *
     * @param \AppBundle\Entity\Station $station
     *
     * @return City
     */
    public function addStation(\AppBundle\Entity\Station $station)
    {
        $this->stations[] = $station;

        return $this;
    }

    /**
     * Remove station
     *
     * @param \AppBundle\Entity\Station $station
     */
    public function removeStation(\AppBundle\Entity\Station $station)
    {
        $this->stations->removeElement($station);
    }

    /**
     * Get stations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStations()
    {
        return $this->stations;
    }
}
