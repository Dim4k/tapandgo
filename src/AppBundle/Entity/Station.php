<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Station
 *
 * @ORM\Table(name="station")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StationRepository")
 *
 * @Assert\Callback(methods={"isAvailableLessThanCapacity"})
 */
class Station
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="creationDate", type="datetime")
	 */
	private $creationDate;

	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="lastUpdate", type="datetime", nullable=true)
	 */
	private $lastUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=1000)
     */
    private $address;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=5000, nullable=true)
	 */
	private $description;

    /**
     * @var float
	 *
     * @ORM\Column(name="latitude", type="float")
	 *
	 * 	@Assert\Range(
	 *      min = -90,
	 *      max = 90,
	 *      minMessage = "Latitude must be at least {{ limit }}",
	 *      maxMessage = "Latitude must be less than {{ limit }}"
	 * )
     */
    private $latitude;

    /**
     * @var float
	 *
     * @ORM\Column(name="longitude", type="float")
	 *
	 * @Assert\Range(
	 *      min = -180,
	 *      max = 180,
	 *      minMessage = "Longitude must be at least {{ limit }}",
	 *      maxMessage = "Longitude must be less than {{ limit }}"
	 * )
     */
    private $longitude;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="bikesCapacity", type="integer")
	 */
	private $bikesCapacity;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="bikesAvailable", type="integer")
	 */
	private $bikesAvailable;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

	/**
	 * @ORM\ManyToOne(targetEntity="City", inversedBy="stations")
	 * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
	 */
	private $city;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Station
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Station
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Station
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Station
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Station
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Station
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Station
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Station
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Station
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set bikesCapacity
     *
     * @param integer $bikesCapacity
     *
     * @return Station
     */
    public function setBikesCapacity($bikesCapacity)
    {
        $this->bikesCapacity = $bikesCapacity;

        return $this;
    }

    /**
     * Get bikesCapacity
     *
     * @return integer
     */
    public function getBikesCapacity()
    {
        return $this->bikesCapacity;
    }

    /**
     * Set bikesAvailable
     *
     * @param integer $bikesAvailable
     *
     * @return Station
     */
    public function setBikesAvailable($bikesAvailable)
    {
        $this->bikesAvailable = $bikesAvailable;

        return $this;
    }

    /**
     * Get bikesAvailable
     *
     * @return integer
     */
    public function getBikesAvailable()
    {
        return $this->bikesAvailable;
    }

	/**
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context, $payload)
	{
		//Add custom controls on entity validation
		if($this->getBikesAvailable() > $this->getBikesCapacity()){
			$context->buildViolation('It can\'t be more bike available thant bike capacity')
				->atPath('bikesCapacity')
				->addViolation();
		}
	}
}
