<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Station;
use AppBundle\Entity\City;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * ApiStation controller.
 */
class ApiStationController extends Controller
{
    /**
     * Lists all station entities.
     *
     * @Route("/stations", name="apistation_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
		//Select all stations with status = 1 (active stations)
		$stations = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:Station')
			->findBy(array('status' => 1 ), null, $limit = $request->query->get('limit'), $limit = $request->query->get('page'));
		/* @var $stations Station[] */

		$formatted = [];
		foreach ($stations as $station) {
			$formatted[] = [
				'id' => $station->getId(),
				'creationDate' => $station->getCreationDate()->format('Y-m-d\TH:i:sP'),
				'lastUpdate' => $station->getLastUpdate() != null ? $station->getLastUpdate()->format('Y-m-d\TH:i:sP'):null,
				'name' => $station->getName(),
				'description' => $station->getDescription(),
				'latitude' => $station->getLatitude(),
				'longitude' => $station->getLongitude(),
				'bikesCapacity' => $station->getBikesCapacity(),
				'bikesAvailable' => $station->getBikesAvailable()
			];
		}

		return new JsonResponse($formatted);
    }

    /**
     * Finds a station entity.
     *
     * @Route("station/{id}", name="apistation_show")
     * @Method("GET")
     */
    public function showAction(Station $station)
    {
		$formatted = [
			'id' => $station->getId(),
			'creationDate' => $station->getCreationDate()->format('Y-m-d\TH:i:sP'),
			'lastUpdate' => $station->getLastUpdate() != null ? $station->getLastUpdate()->format('Y-m-d\TH:i:sP'):null,
			'name' => $station->getName(),
			'description' => $station->getDescription(),
			'latitude' => $station->getLatitude(),
			'longitude' => $station->getLongitude(),
			'bikesCapacity' => $station->getBikesCapacity(),
			'bikesAvailable' => $station->getBikesAvailable()
		];

		return new JsonResponse($formatted);
    }

	/**
	 * Finds stations entities in a city.
	 *
	 * @Route("stations/{id}", name="apistation_stationsincity")
	 * @Method("GET")
	 */
	public function getStationsInCityAction(Request $request, $id)
	{
		//In order to fetch the required routing, we need to check the parameter after station in the uri.
		if($id == 'near'){
			$response = $this->forward('AppBundle:Api\ApiStation:getNearestStations', array(
				'id'  => $id,
				'lat' => $request->query->get('lat'),
				'lng' => $request->query->get('lng'),
				'radius' => $request->query->get('radius'),
			));
			return $response;
		} else {
			$stations = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:Station')
				->findBy(array('status' => $id), null, $limit = $request->query->get('limit'), $limit = $request->query->get('page'));

			$formatted = [];

			/* @var $stations Station[] */
			foreach ($stations as $station) {
				$formatted[] = [
					'id' => $station->getId(),
					'creationDate' => $station->getCreationDate()->format('Y-m-d\TH:i:sP'),
					'lastUpdate' => $station->getLastUpdate() != null ? $station->getLastUpdate()->format('Y-m-d\TH:i:sP') : null,
					'name' => $station->getName(),
					'description' => $station->getDescription(),
					'latitude' => $station->getLatitude(),
					'longitude' => $station->getLongitude(),
					'bikesCapacity' => $station->getBikesCapacity(),
					'bikesAvailable' => $station->getBikesAvailable()
				];
			}

			return new JsonResponse($formatted);
		}
	}

	/**
	 * Finds stations from coordinates.
	 *
	 * @Route("nearstations", name="apistation_nearstations")
	 * @Method("GET")
	 */
	public function getNearestStationsAction(Request $request,$lat,$lng,$radius)
	{
		//Check for parameters if not from action forward
		$lat = $lat != null ? $lat : $request->query->get('lat');
		$lng = $lng != null ? $lng : $request->query->get('lng');
		$radius = $radius != null ? $radius : $request->query->get('radius');

		$em = $this->getDoctrine()->getManager();

		//We use Geo/Spatial search with MySQL (https://developers.google.com/maps/solutions/store-locator/clothing-store-locator#finding-locations-with-mysql)
		$RAW_QUERY = 'SELECT *, ( 6371  * acos( cos( radians(:lat) ) * cos( radians( latitude ) ) *
				cos( radians( longitude ) - radians(:lng) ) + sin( radians(:lat) ) *
				sin( radians( latitude ) ) ) ) AS distance
				FROM Station
				WHERE status = 1
				HAVING distance < :radius
				ORDER BY distance';

		$publishDate = \DateTime::createFromFormat('Y-m-d H:i:s', '1/10/2014');

		$statement = $em->getConnection()->prepare($RAW_QUERY);
		// Set parameters
		$statement->bindValue('lat', $lat);
		$statement->bindValue('lng', $lng);
		$statement->bindValue('radius', $radius);
		$statement->execute();

		$result = $statement->fetchAll();

		$formatted = [];
		foreach($result as $station) {
			$station['lastUpdate'] = $station['lastUpdate'] != null ? \DateTime::createFromFormat('Y-m-d H:i:s', $station['lastUpdate'])->format('Y-m-d\TH:i:sP'):null;
			$station['creationDate'] = \DateTime::createFromFormat('Y-m-d H:i:s', $station['creationDate'])->format('Y-m-d\TH:i:sP');

			$formatted[] = [
				'id' => $station['id'],
				'creationDate' => $station['creationDate'],
				'lastUpdate' => $station['lastUpdate'],
				'name' => $station['name'],
				'description' => $station['description'],
				'latitude' => $station['latitude'],
				'longitude' => $station['longitude'],
				'bikesCapacity' => $station['bikesCapacity'],
				'bikesAvailable' => $station['bikesAvailable']
			];
		}

		return new JsonResponse($formatted);
	}

	/**
	 * Remove a bike from station and return true on success.
	 *
	 * @Route("stations/{id}/take", name="apistation_takebike")
	 * @Method("POST")
	 */
	public function takeBikeAction(Request $request, Station $station)
	{
		//Check if there is enough bike available in the station
		$response = $station->getBikesAvailable() >= 1 ? ['result'=>true] : ['result'=>false];

		if($response{'result'}){
			$station->setBikesAvailable($station->getBikesAvailable()-1);
			$em = $this->getDoctrine()->getManager();
			$em->persist($station);
			$em->flush();
		}

		return new JsonResponse($response);
	}

	/**
	 * Add a bike to a station if the bike capacity is not reach and return true on success.
	 *
	 * @Route("stations/{id}/drop", name="apistation_dropbike")
	 * @Method("POST")
	 */
	public function dropBikeAction(Request $request, Station $station)
	{
		//Check if the bike capacity is superior to the bike available
		$response = $station->getBikesAvailable() < $station->getBikesCapacity() ? ['result'=>true] : ['result'=>false];

		if($response{'result'}){
			$station->setBikesAvailable($station->getBikesAvailable()+1);
			$em = $this->getDoctrine()->getManager();
			$em->persist($station);
			$em->flush();
		}

		return new JsonResponse($response);
	}
}
