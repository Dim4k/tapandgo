<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\City;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * ApiCity controller.
 */
class ApiCityController extends Controller
{

	/**
	 * Lists all city entities.
	 *
	 * @Route("/cities", name="apicity_index")
	 * @Method("GET")
	 */
	public function getCitiesAction(Request $request)
	{
		//Select all cities with status = 1 (active cities)
		$cities = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:City')
			->findBy(array('status' => 1 ), null, $limit = $request->query->get('limit'), $limit = $request->query->get('page'));
		/* @var $cities City[] */

		$formatted = [];
		foreach ($cities as $city) {
			$formatted[] = [
				'id' => $city->getId(),
				'name' => $city->getName(),
				'latitude' => $city->getLatitude(),
				'longitude' => $city->getLongitude(),
			];
		}

		return new JsonResponse($formatted);
	}

	/**
	 * Finds and displays a city entity.
	 *
	 * @Route("/city/{id}", name="apicity_show")
	 * @Method("GET")
	 */
	public function getCityAction(City $city)
	{
		$formatted = [
			'id' => $city->getId(),
			'name' => $city->getName(),
			'latitude' => $city->getLatitude(),
			'longitude' => $city->getLongitude(),
		];

		return new JsonResponse($formatted);
	}

}
