<?php

namespace AppBundle\Tests;

use AppBundle\Entity\City;
use AppBundle\Entity\Station;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Input\ArrayInput;
use Doctrine\Bundle\DoctrineBundle\Command\DropDatabaseDoctrineCommand;
use Doctrine\Bundle\DoctrineBundle\Command\CreateDatabaseDoctrineCommand;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\CreateSchemaDoctrineCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Console\Tester\CommandTester;


class AppBundleMainTests extends WebTestCase
{
	/**
	 * @var EntityManager
	 */
	private $_em;

	private $application;

	// Initialize test database and some test data
	protected function setUp()
	{
		static::$kernel = static::createKernel();
		static::$kernel->boot();

		$this->application = new Application(static::$kernel);

		// drop the database
		$command = new DropDatabaseDoctrineCommand();
		$this->application->add($command);
		$input = new ArrayInput(array(
			'command' => 'doctrine:database:drop',
			'--force' => true
		));
		$command->run($input, new NullOutput());

		// we have to close the connection after dropping the database so we don't get "No database selected" error
		$connection = $this->application->getKernel()->getContainer()->get('doctrine')->getConnection();
		if ($connection->isConnected()) {
			$connection->close();
		}

		// create the database
		$command = new CreateDatabaseDoctrineCommand();
		$this->application->add($command);
		$input = new ArrayInput(array(
			'command' => 'doctrine:database:create',
		));
		$command->run($input, new NullOutput());

		// create schema
		$command = new CreateSchemaDoctrineCommand();
		$this->application->add($command);
		$input = new ArrayInput(array(
			'command' => 'doctrine:schema:create',
		));
		$command->run($input, new NullOutput());

		// get the Entity Manager
		$this->_em = static::$kernel->getContainer()
			->get('doctrine')
			->getManager();

		$city1 = new City();
		$city1->setStatus(true);
		$city1->setLatitude(47.2361027);
		$city1->setLongitude(-1.6266626);
		$city1->setName('Nantes');
		$this->_em->persist($city1);

		$city2 = new City();
		$city2->setStatus(true);
		$city2->setLatitude(48.1159102);
		$city2->setLongitude(-1.7234738);
		$city2->setName('Rennes');
		$this->_em->persist($city2);

		$station1 = new Station();
		$date1 = new \DateTime('2016-02-01');
		$station1->setName('Nantes Station n1');
		$station1->setLatitude(47.205466);
		$station1->setLongitude(-1.5255087);
		$station1->setStatus(true);
		$station1->setBikesCapacity(15);
		$station1->setBikesAvailable(14);
		$station1->setAddress('15 Rue du test');
		$station1->setCity($city1);
		$station1->setCreationDate($date1);
		$this->_em->persist($station1);

		$station2 = new Station();
		$date2 = new \DateTime('2017-03-01');
		$station2->setName('Nantes Station n2');
		$station2->setLatitude(47.1997892);
		$station2->setLongitude(-1.5639302);
		$station2->setStatus(true);
		$station2->setBikesCapacity(15);
		$station2->setBikesAvailable(12);
		$station2->setAddress('30 Rue du test');
		$station2->setCity($city1);
		$station2->setCreationDate($date2);
		$this->_em->persist($station2);

		$station3 = new Station();
		$date3 = new \DateTime('2017-05-01');
		$station3->setName('Rennes Station n1');
		$station3->setLatitude(47.1997892);
		$station3->setLongitude(-1.5639302);
		$station3->setStatus(true);
		$station3->setBikesCapacity(25);
		$station3->setBikesAvailable(20);
		$station3->setAddress('58 Rue du test');
		$station3->setCity($city2);
		$station3->setCreationDate($date3);
		$this->_em->persist($station2);


		$this->_em->flush();
		$this->_em->clear();

	}

	protected function tearDown()
	{
		parent::tearDown();
		$this->_em->close();
	}

	// Tests begin from here
	public function testHomepage()
	{
		$client = static::createClient();

		$crawler = $client->request('GET', '/');

		$this->assertEquals(200, $client->getResponse()->getStatusCode(),'Unexpected status code response ');
		$this->assertContains('Welcome to TapAndGo',
			$crawler->filter('body h1')->text(),
			'Unexpected content response');
	}

	public function testGetCities()
	{
		$client = $this->createClient();
		$client->request('GET', '/api/cities');

		$response = $client->getResponse();

		// Test if response is OK
		$this->assertSame(200, $client->getResponse()->getStatusCode(),'Unexpected status code response ');
		// Test if Content-Type is valid application/json
		$this->assertSame('application/json', $response->headers->get('Content-Type'),'Unexpected content type response');
		// Test response content fetch json format and test data
		$this->assertJsonStringEqualsJsonString($client->getResponse()->getContent(),
			'[{"id":1,"name":"Nantes","latitude":47.2361027,"longitude":-1.6266626},{"id":2,"name":"Rennes","latitude":48.1159102,"longitude":-1.7234738}]',
			'Unexpected Json response');
	}

	public function testGetStationsByCity()
	{
		$client = $this->createClient();

		// Test with dates parameters
		$client->request('GET', '/api/stations/1');

		$response = $client->getResponse();

		// Test if response is OK
		$this->assertSame(200, $client->getResponse()->getStatusCode());
		// Test if Content-Type is valid application/json
		$this->assertSame('application/json', $response->headers->get('Content-Type'));
		// Test response content fetch json format and test data
		$this->assertJsonStringEqualsJsonString($client->getResponse()->getContent(),
			'[{"id":1,"creationDate":"2016-02-01T00:00:00+00:00","lastUpdate":null,"name":"Nantes Station n1","description":null,"latitude":47.205466,"longitude":-1.5255087,"bikesCapacity":15,"bikesAvailable":14},{"id":2,"creationDate":"2017-03-01T00:00:00+00:00","lastUpdate":null,"name":"Nantes Station n2","description":null,"latitude":47.1997892,"longitude":-1.5639302,"bikesCapacity":15,"bikesAvailable":12}]',
			'Unexpected Json response');
	}


	public function testTakeBikePost()
	{
		$client = $this->createClient();

		// Test with dates parameters
		$client->request('POST', 'api/stations/1/take');

		$response = $client->getResponse();

		// Test if response is OK
		$this->assertSame(200, $client->getResponse()->getStatusCode());

		$station1 = $this->_em->getRepository('AppBundle:Station')->find(1);

		$this->assertEquals($station1->getBikesAvailable(),
			13,
			$client->getResponse()->getContent());
	}

}
